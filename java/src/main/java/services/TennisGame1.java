package services;
import entities.Player;
import managers.TennisGameManager;

public class TennisGame1 implements TennisGame {
    private TennisGameManager tennisGameManager;

    public TennisGame1(String player1Name, String player2Name) {
        tennisGameManager = new TennisGameManager(new Player(player1Name), new Player(player2Name));
        tennisGameManager.updateScore();
    }

    public void wonPoint(String playerName) {
        tennisGameManager.getPlayerByName(playerName)
                .wonPoint();
        tennisGameManager.updateScore();
    }

    public String getScore() {
        return tennisGameManager.getScore()
                .toString();
    }

}
