package entities;
public class EqualScore extends Score {

    public EqualScore(Player player1, Player player2) {
        super(player1, player2);
    }

    public String toString() {
        return EqualScoreEnum.valueOf(player1.getPoints())
                .toString();
    }

}
