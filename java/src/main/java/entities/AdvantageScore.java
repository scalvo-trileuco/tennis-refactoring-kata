package entities;
public class AdvantageScore extends Score {
    private static final int ADVANTAGE_THRESHOLD = 1;

    public AdvantageScore(Player player1, Player player2) {
        super(player1, player2);
    }

    private Player getAdvantageousPlayer() {
        return ((player1.getPoints() - player2.getPoints()) == ADVANTAGE_THRESHOLD ? player1 : player2);
    }

    public String toString() {
        return "Advantage " + getAdvantageousPlayer().getName();

    }

}
