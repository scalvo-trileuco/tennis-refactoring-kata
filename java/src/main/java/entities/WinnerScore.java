package entities;

public class WinnerScore extends Score {
    private static final int WINNER_THRESHOLD = 2;

    public WinnerScore(Player player1, Player player2) {
        super(player1, player2);
    }

    private Player getWinner() {
        return ((player1.getPoints() - player2.getPoints()) >= WINNER_THRESHOLD ? player1 : player2);
    }

    @Override
    public String toString() {
        return "Win for " + getWinner().getName();
    }

}
