package entities;

public enum EqualScoreEnum {

    LOVE_ALL("Love-All"),
    FIFTEEN_ALL("Fifteen-All"),
    THIRTY_ALL("Thirty-All"),
    DEUCE("Deuce");

    private final String name;

    private EqualScoreEnum(String string) {
        this.name = string;
    }

    public static EqualScoreEnum valueOf(int value) {
        switch (value) {
        case 0:
            return EqualScoreEnum.LOVE_ALL;
        case 1:
            return EqualScoreEnum.FIFTEEN_ALL;
        case 2:
            return EqualScoreEnum.THIRTY_ALL;
        default:
            return EqualScoreEnum.DEUCE;
        }
    }

    @Override
    public String toString() {
        return this.name;
    }

}