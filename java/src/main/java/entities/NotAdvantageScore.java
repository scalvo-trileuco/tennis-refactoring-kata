package entities;
public class NotAdvantageScore extends Score {
    public NotAdvantageScore(Player player1, Player player2) {
        super(player1, player2);
    }

    public String toString() {
        return NotAdvantageScoreEnum.valueOf(player1.getPoints())
                .toString() + "-"
                + NotAdvantageScoreEnum.valueOf(player2.getPoints())
                        .toString();
    }

}
