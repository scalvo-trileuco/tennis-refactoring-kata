package entities;

public class Player {
    private int points;
    private String name;

    public Player(String name) {
        this.name = name;
        this.points = 0;
    }

    public int getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public void wonPoint() {
        this.points++;
    }

    public boolean hasAdvantage() {
        return (points >= 4);
    }

}
