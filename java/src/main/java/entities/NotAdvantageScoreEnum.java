package entities;

public enum NotAdvantageScoreEnum {

    LOVE("Love"),
    FIFTEEN("Fifteen"),
    THIRTY("Thirty"),
    FORTY("Forty");

    private final String name;

    private NotAdvantageScoreEnum(String string) {
        this.name = string;
    }

    public static NotAdvantageScoreEnum valueOf(int value) {
        switch (value) {
        case 0:
            return NotAdvantageScoreEnum.LOVE;
        case 1:
            return NotAdvantageScoreEnum.FIFTEEN;
        case 2:
            return NotAdvantageScoreEnum.THIRTY;
        default:
            return NotAdvantageScoreEnum.FORTY;
        }
    }

    @Override
    public String toString() {
        return this.name;
    }

}