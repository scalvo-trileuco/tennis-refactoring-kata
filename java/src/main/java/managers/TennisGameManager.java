package managers;

import entities.AdvantageScore;
import entities.EqualScore;
import entities.NotAdvantageScore;
import entities.Player;
import entities.Score;
import entities.WinnerScore;

public class TennisGameManager {
    private Player player1;
    private Player player2;
    private Score scoreBoard;

    public TennisGameManager(Player player1, Player player2) {
        super();
        this.player1 = player1;
        this.player2 = player2;
    }

    public Player getPlayerByName(String playerName) {
        return player1.getName()
                .equals(playerName) ? player1 : player2;
    }

    public void updateScore() {
        if (player1.getPoints() == player2.getPoints()) {
            scoreBoard = new EqualScore(player1, player2);
        } else if (anyoneHasAdvantage() && anyoneHasOneMorePoint()) {
            scoreBoard = new AdvantageScore(player1, player2);
        } else if (anyoneHasAdvantage() && !anyoneHasOneMorePoint()) {
            scoreBoard = new WinnerScore(player1, player2);
        } else {
            scoreBoard = new NotAdvantageScore(player1, player2);
        }
    }

    private boolean anyoneHasOneMorePoint() {
        return ((Math.abs(player1.getPoints() - player2.getPoints()) == 1));
    }

    private boolean anyoneHasAdvantage() {
        return (player1.hasAdvantage() || player2.hasAdvantage());
    }

    public String getScore() {
        return scoreBoard.toString();
    }
}
